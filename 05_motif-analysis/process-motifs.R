library(data.table)
library(plyr)
library(tidyverse)
library(Biostrings)
library(DESeq2)

dir.create("plots", showWarnings = FALSE)
dir.create("datafiles", showWarnings = FALSE)


print("load gene expression data")
dds.rna <- readRDS("datafiles/dds-rna-selected-condition-batchcorr.rds")
dds.rna.counts <- counts(dds.rna, normalize = TRUE)
expressed.genes.old <- names(which(rowMeans(dds.rna.counts) > 10))
print("select expressed genes with at least 200 reads in at least one condition in at least one study")
min.count <- 200
dds.rna.counts.dt <- as.data.table(dds.rna.counts)
dds.rna.counts.dt$name <- rownames(dds.rna)
dds.rna.counts.dt <- gather(dds.rna.counts.dt, key = "sample",
                            value = "count", -name)
dds.rna.counts.dt <- as.data.table(dds.rna.counts.dt)
dds.rna.coldata <- colData(dds.rna)
dds.rna.coldata$sample <- rownames(dds.rna.coldata)
dds.rna.counts.dt <-
    merge(dds.rna.counts.dt,
          as.data.table(dds.rna.coldata)[, .(sample, condition, datasource)],
          by = "sample")
dds.rna.counts.dt <-
    dds.rna.counts.dt[, .(mean.count = mean(count)),
                      by = c("name", "condition", "datasource")]
dds.rna.counts.dt <- dds.rna.counts.dt[mean.count > min.count]
expressed.genes <- sort(unique(dds.rna.counts.dt[, name]))


print("load TF and motif information")
motif.dirname <-
    "~/Projects/DNaseTcell/data/cisbp-motifs/Mus_musculus_2016_06_01_2-46_pm"
tf.info.all <-
    fread(sprintf("%s/TF_Information_all_motifs_plus.txt", motif.dirname))
colnames(tf.info.all)[6] <- "TF.DBID"
colnames(tf.info.all)[14] <- "Motif.DBID"

print("filter motifs")
tf.info <- tf.info.all[TF_Name %in% expressed.genes]
# tf.info <- tf.info[(Cutoff >= 0.5) | (TF_Status == "D")]

print("load PWMs")
pwms <- list()
motif.ids <- unique(tf.info$Motif_ID)
for (motif.id in motif.ids) {
    filename <- sprintf("%s/pwms_all_motifs/%s.txt", motif.dirname, motif.id)
    if (file.exists(filename)) {
        motif <- fread(filename)
        if (nrow(motif) > 0) {
            pwm <- t(as.matrix(as.data.frame(motif)[,2:5]))
            pwms[[motif.id]] <- pwm
        }
    }
}

print("keep only PWMs of high complexity")
pwm.maxscore <- sapply(pwms, maxScore)
pwms <- pwms[pwm.maxscore > 5]


tf.info <- tf.info[Motif_ID %in% names(pwms)]
motif.tf.name <- tf.info[, .(TF_Name, Motif_ID)]
motif.tf.name <- unique(motif.tf.name)
motif.tf.name <- motif.tf.name[, .(Motif_Name = paste(sort(TF_Name),
                                                      collapse = ",")),
                               by = Motif_ID]
tf.info <- merge(tf.info, motif.tf.name, by = "Motif_ID")

print(sprintf("%s PWMs processed and saved", length(pwms)))
saveRDS(pwms, "datafiles/cisbp-v1.02-mouse-pwms.rds")
print("TF information processed and saved")
saveRDS(tf.info, "datafiles/cisbp-v1.02-mouse-tfinfo-expressed.rds")


print("save PWMs in HOMER format")
printMotifHOMER <- function(pwm, name, file, scoreScale = 0.7) {
    # write motif to file according to HOMER format
    score <- maxScore(pwm) * scoreScale
    motif.name <- tf.info[Motif_ID == name][, Motif_Name]
    motif.name <- unique(motif.name)
    motif.name <- sprintf("%s_%s", name, motif.name)
    write(sprintf(">%s\t%s\t%s", name, motif.name, score), file, append = TRUE)
    write.table(format(t(pwm), digits = 4), file, sep = "\t", quote = FALSE,
                row.names = FALSE, col.names = FALSE, append = TRUE)
}
motifs.filename <- "datafiles/cisbp-v1.02-mouse-expressed.motifs"
file.remove(motifs.filename)
tmp <- mapply(printMotifHOMER, pwms, names(pwms), motifs.filename)


print("save PWMs in MEME format")
printMotifMEME <- function(pwm, name, file) {
    motif.name <- tf.info[Motif_ID == name][, Motif_Name]
    motif.name <- unique(motif.name)
    motif.name <- sprintf("%s_%s", name, motif.name)
    write(sprintf("MOTIF %s", motif.name), file, append = TRUE)
    write(sprintf("letter-probability matrix: alengh= 4 w= %s", ncol(pwm)),
          file, append = TRUE)
    write.table(format(t(pwm)[, c("A", "C", "G", "T")], digits = 4),
                file, sep = "\t",
                quote = FALSE, row.names = FALSE, col.names = FALSE,
                append = TRUE)
    write("", file, append = TRUE)
}
meme.filename <- "datafiles/cisbp-v1.02-mouse-expressed.meme"
file.remove(meme.filename)
write("MEME version 4\n", meme.filename, append = TRUE)
write("ALPHABET= ACGT\n", meme.filename, append = TRUE)
write("strands: + -\n", meme.filename, append = TRUE)
background <- c('A' = 0.25, 'C' = 0.25, 'G' = 0.25, 'T' = 0.25)
write("Background letter frequencies", meme.filename, append = TRUE)
write(paste0(sapply(c("A", "C", "G", "T"),
                    function(l) paste(c(l, background[l]))),
             collapse = " "), meme.filename, append = TRUE)
write("", meme.filename, append = TRUE)
tmp <- mapply(printMotifMEME, pwms, names(pwms), meme.filename)


sessionInfo()
