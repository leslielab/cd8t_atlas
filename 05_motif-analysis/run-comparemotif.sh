#!/bin/bash

motifsfile="datafiles/cisbp-v1.02-mouse-expressed.motifs"
homerdir="homer-denovo-output"

mkdir -p $homerdir/homerResults_extended

compareMotifs.pl $homerdir/homerMotifs.all.motifs $homerdir/homerResults_extended -reduceThresh 0.7 -matchThresh 0.9 -bits -cpu 10 -known $motifsfile >.compare-motifs 2>.err.compare-motifs
