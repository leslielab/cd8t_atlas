#!/bin/bash

motifs="datafiles/cisbp-v1.02-mouse-expressed-selected-extended.meme"
seqs="datafiles/selected-summits.fasta"
fimodir="fimo-output-selected"
mkdir -p $fimodir

fimooutput="$fimodir/fimo-hits-selected-summits.txt"
fimo --no-qvalue --verbosity 1 --max-strand --thresh 0.001 --text --skip-matched-sequence $motifs $seqs >$fimooutput
