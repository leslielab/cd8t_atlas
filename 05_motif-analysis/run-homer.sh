#!/bin/bash

summits="datafiles/selected-summits.bed"
motifsfile="datafiles/cisbp-v1.02-mouse-expressed.motifs"
homerdir="homer-denovo-output"

findMotifsGenome.pl $summits mm10 $homerdir -len 8,10,12 -size given -S 100 -N 1000000 -mcheck $motifsfile -mknown $motifsfile -bits -p 10 -cache 1000 >.homer-output 2>.err.homer-output
