# Computational analysis for the manuscript `A unified atlas of CD8 T cell dysfunctional states in cancer and infection`, Pritykin\*, van der Veeken\* *et al.*


- Folder `01_collect-data`:
  Collect and preprocess published bulk ATAC-seq and RNA-seq data.

  - Download ATAC-seq data from GEO.
  Example script: `dump-files-atac-pauken2016.sh`

  - Align ATAC-seq reads to mouse genome using bowtie2.
  Example script: `run-alignment-atac-pauken2016.sh`

  - Call peaks using MACS2 for individual ATAC-seq replicates and for all replicates merged.
  Example script: `peakcalling-pauken2016/peak-calling.sh`

  - Reproducibility analysis of ATAC-seq peaks using IDR.
  Example script: `peakcalling-pauken2016/idr-analysis.sh`

  - Download RNA-seq data from GEO.
  Example script: `dump-files-rna-pauken2016.sh`

  - Align RNA-seq reads to mouse genome using hisat2.
  Example script: `run-alignment-rna-pauken2016.sh`

  - Count RNA-seq reads in genes.
  Example script: `rna-count-genes-pauken2016.R`


- Folder `02_combined-atlas`:
  Construct ATAC-seq peak atlas.

  - Combine ATAC-seq peaks from all experiments.
    Script: `compare-combine-peaks.R`

  - Produce bigWig files with ATAC signal coverage for each data source.
    Script: `produce-bigwig-individual.sh`

  - Find summits using package `biosignals`.
    Script: `find-summits.R`

  - Count reads in peaks and summits.
    Script: `count-reads.R`

  - Organize meta-data for ATAC-seq read counts.
    Script: `process-counts.R`

  - Filter peaks and summits: filter out summits with too low a read count and select peaks that contain at least one summit.
    Script: `filter-peaks-summits.R`


- Folder `03_batch-deseq`:
  GLM-based batch effect correction.

  - Learn GLMs for batch effect correction and apply the correction to read counts.
    Scripts: `batch-correction-deseq-atac-peaks.R` for analysis of counts in peaks and `batch-correction-deseq-atac-summits.R` for summits.

  - The same analysis but including data from hybrid mice.
    Script: `batch-correction-deseq-atac-peaks-hybrid.R`.
    Results: files `dds-peaks-withhybrid-uncorrected.rds`, `dds-peaks-withhybrid-condition-batchcorr.rds`.

  - The same analysis as above but including additional samples.
    Scripts: `batch-correction-deseq-atac-peaks-tissueres.R` for including tissure-resident memory T cell samples, `batch-correction-deseq-atac-peaks-effmem.R` for terminal effector and memory precursor cells.


- Folder `04_combined-atlas-geneexpr`:
  Preprocessing and batch effect correction of RNA-seq data.

  - Preprocess all RNA-seq read counts and organize meta-data.
    Script: `preprocess-rna-counts.R`

  - Batch effect correction of RNA-seq data using GLMs.
    Script: `batch-correction-deseq-rna.R`


- Folder `05_motif-analysis`:
  TF motif analysis in ATAC-seq peaks.

  - Prepare peak summit region sequences.
    Script: `prepare-summits.R`

  - Prepare motifs for TFs encoded by expressed genes.
    Script: `process-motifs.R`

  - Run HOMER to identify motifs enriched in peaks vs. background.
    Scripts: `run-homer.sh` to find *de novo* motifs and `run-comparemotif.sh` to rerun comparison of *de novo* identified motifs with known motifs.

  - Identify motifs significantly enriched in peaks vs. background sequences.
    Script: `select-signif-motifs-homer.R`.
    Results: file `homer-motifs-selected.rds` with TF motif information for the selected significant motifs.

  - Run FIMO to find motif hits in regions around summits.
    Script: `run-fimo-selected.sh`

  - Process hits of selected significant motifs.
    Script: `select-signif-hits-fimo.R`

  - Analyze and then merge some very similar motifs.
    Script: `analyze-merge-rename-motif-hits.R`


- Folder `06_model-accessibility`:
  GLM NB regression of ATAC-seq accessibility via TF motif hits.

  - Prepare data for GLM NB fits on a cluster.
    Script: `prepare-glmreg-run.R`

  - Run GLM NB fits on a cluster.
    Scripts: `glmreg-one-sample.R` to run an individual sample and master script `run-glmreg-all.sh` to run all fits.


- Folder `07_diff-accessibility`:
  Differential accessibility analysis of bulk ATAC-seq data.

  - Pairwise differential accessibility analysis for major comparisons.
    Scripts: `peak-diff-accessibility-deseq.R` for differential accessibility of peaks and `peak-summit-diff-accessibility-deseq.R` for differential accessibility of peak summits.

  - Differential accessibility for peaks consolidated at the gene level.
    Script: `gene-diff-accessibility.R`

  - Calculate ATAC-seq signatures.
    Script: `calculate-atac-signatures.R`


- Folder `08_diff-expression`:
  Differential expression analysis of bulk RNA-seq data.

  - Pairwise differential gene expression analysis for major comparisons.
    Script: `diff-expression-deseq.R`

