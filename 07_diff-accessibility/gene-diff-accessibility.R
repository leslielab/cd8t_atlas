library(rtracklayer)
library(GenomicRanges)
library(tidyverse)
library(data.table)
library(DESeq2)
library(qvalue)


dir.create("plots", showWarnings = FALSE)
dir.create("plots/pca", showWarnings = FALSE)
dir.create("datafiles", showWarnings = FALSE)


print("load data")
summits <- readRDS("datafiles/selected-summits.rds")


tags <- c("naive-effector", "effector-memory", "naive-memory",
          "naive-dysfunc", "naive-func", "func-dysfunc", "naive-dysfuncearly",
          "naive-dysfuncearly-lcmv", "naive-dysfuncearly-hepatocarc",
          "effector-dysfuncearly",
          "memory-dysfunclate", 
          "dysfunc-earlylate", "dysfunc-earlylate-lcmv",
          "dysfunc-earlylate-hepatocarc",
          "dysfunc-progenitor-terminal", "dysfunc-progenitor-terminal-melanoma",
          "dysfunc-progenitor-terminal-lcmv",
          "dysfunc-progenitor-terminal-melanoma-cart",
          "dysfunc-progenitor-terminal-melanoma-chen2019",
          "dysfunc-progenitor-terminal-melanoma-miller2019",
          "TE-MPEC", "naive-MPEC", "MPEC-memory", "MPEC-dysfuncprog-lcmv",
          "dysfunc-tumor-chronic-late", "dysfunc-tumor-chronic",
          "dysfunc-hepatocarc-chronic", "dysfunc-melanoma-chronic",
          "dysfunc-cart-endogenous-progenitor",
          "dysfunc-cart-endogenous-terminal")


all.res <- sapply(tags,
                  function(tag) {
                      filename <- sprintf("datafiles/res-summits-%s.rds", tag)
                      readRDS(filename)
                  }, simplify = FALSE)


all.genes <- unique(subset(summits, !is.na(gene))$gene)


print("detect trend of differential accessibility")

calculateGeneTrend <- function(res.dt) {
    trend.gene <-
        data.table(gene = all.genes,
                   npeaks = 0, peak.log2FC.mean = NA, peak.log2FC.med = NA,
                   peak.log2FC.mwu.p = NA)
    trend.gene[, peak.log2FC.mean := as.numeric(peak.log2FC.mean)][,
                    peak.log2FC.med := as.numeric(peak.log2FC.med)][,
                    peak.log2FC.mwu.p := as.numeric(peak.log2FC.mwu.p)]
    setkey(trend.gene, key = "gene")
    all.peak.log2FC <- res.dt$log2FoldChange
    for (gene.name in all.genes) {
        peaks.gene <- res.dt[gene == gene.name]
        npeaks <- nrow(peaks.gene)
        trend.gene[gene.name]$npeaks <- npeaks
        trend.gene[gene.name]$peak.log2FC.mean <-
            mean(peaks.gene$log2FoldChange)
        trend.gene[gene.name]$peak.log2FC.med <-
            median(peaks.gene$log2FoldChange)
        if (npeaks > 1) {
            trend.gene[gene.name]$peak.log2FC.mwu.p <-
                wilcox.test(peaks.gene$log2FoldChange, all.peak.log2FC)$p.value
        }
    }
    trend.gene$peak.log2FC.mwu.q <- qvalue(trend.gene$peak.log2FC.mwu.p)$qvalue
    gene.best.peak <- res.dt[!is.na(padj)][, .(log2FoldChange, padj,
                                               best.peak.padj = min(padj)),
                                           by = gene]
    gene.best.peak <-
        gene.best.peak[padj == best.peak.padj][,
                   .(gene, best.peak.log2FC = log2FoldChange, best.peak.padj)]
    gene.best.peak <- unique(gene.best.peak, by = "gene")
    trend.gene <- merge(trend.gene, gene.best.peak, by = "gene", all.x = TRUE)
}


for (tag in tags) {
    gene.trend <- calculateGeneTrend(all.res[[tag]])
    saveRDS(gene.trend, sprintf("datafiles/gene-trend-%s.rds", tag))
}




sessionInfo()
