#!/bin/bash

echo "produce bigWig from bedGraph"
echo
bedtools --version
echo


dirname="/Volumes/Drive20/Projects-data/dysfuncTcells/data/"
bedgrdirname="bedgr"
bigwigdirname="bigwig"

mkdir -p $bedgrdirname
mkdir -p $bigwigdirname

bdg_pauken2016="$dirname"/chronic-ATAC-Pauken2016/peakcalling/peaks-macs2/merged/merged_treat_pileup.bdg
bdg_sen2016="$dirname"/chronic-ATAC-Sen2016/peakcalling/peaks-macs2/merged/merged_treat_pileup.bdg
bdg_scottbrowne2016="$dirname"/chronic-ATAC-Mognol2017/peakcalling/peaks-macs2/merged/merged_treat_pileup.bdg
bdg_scharer2017="$dirname"/chronic-ATAC-Schrarer2017/peakcalling/peaks-macs2/merged/merged_treat_pileup.bdg
bdg_mognol2017="$dirname"/chronic-ATAC-Mognol2017/peakcalling/peaks-macs2/merged-TIL-invivo/merged-TIL-invivo_treat_pileup.bdg
bdg_philip2017="$dirname"/tumordysfunc-Philip2017/peakcalling/peaks-macs2/merged2/merged_treat_pileup.bdg
bdg_hybrid_ucsc="$bedgrdirname/atac-signal-hybrid-mouse-ucsc.bdg"
bdg_milner2017="$dirname"/tissueresident-ATAC-Milner2017/peakcalling/peaks-macs2/merged/merged_treat_pileup.bdg
bdg_miller2019="$dirname"/chronic-tumor-ATAC-Miller2019/peakcalling/peaks-macs2/merged/merged_treat_pileup.bdg
bdg_chen2019="$dirname"/til-cart-ATAC-Chen2019/peakcalling/peaks-macs2/merged/merged_treat_pileup.bdg
bdg_yu2017="$dirname"/effmem-ATAC-Yu2017/peakcalling/peaks-macs2/merged/merged_treat_pileup.bdg



peaks="$HOME//Projects/dysfuncTcells/combined-atlas/datafiles/all-atac-peaks.bed"

exprimentnames=(pauken2016 sen2016 scottbrowne2016 scharer2017 mognol2017 philip2017 hybrid_mouse milner2017 miller2019 chen2019 yu2017)
bedgrfiles=($bdg_pauken2016 $bdg_sen2016 $bdg_scottbrowne2016 $bdg_scharer2017 $bdg_mognol2017 $bdg_philip2017 $bdg_hybrid_ucsc $bdg_milner2017 $bdg_miller2019 $bdg_chen2019 $bdg_yu2017)


for i in {0..10}
do
    experiment_name=${exprimentnames[$i]}
    bedgr_file=${bedgrfiles[$i]}

    echo "process $experiment_name"
    echo "start from file $bedgr_file"

    bedgr_file_peaks=$bedgrdirname/"$experiment_name"_peaks.bdg
    bedgr_file_peaks_sorted=$bedgrdirname/"$experiment_name"_peaks_sorted.bdg
    bw=$bigwigdirname/"$experiment_name"_peaks.bw

    echo "run bedtools intersect"
    bedtools intersect -a $bedgr_file -b $peaks >$bedgr_file_peaks
    echo "done"

    echo "run bedtools sort"
    bedtools sort -i $bedgr_file_peaks >$bedgr_file_peaks_sorted
    echo "done"

    echo "run bedGraphToBigWig"
    bedGraphToBigWig $bedgr_file_peaks_sorted mm10.genome_ $bw
    echo "done"
done
