library(rtracklayer)
library(GenomicRanges)
library(tidyverse)
library(data.table)
library(Rsubread)
library(DESeq2)


bam.lcmv.pauken.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/chronic-ATAC-Pauken2016/bam"
bam.lcmv.pauken <- list.files(bam.lcmv.pauken.dir, pattern = "*.bam$")
tag.lcmv.pauken <- sapply(strsplit(bam.lcmv.pauken, "\\."), "[", 1)
stopifnot(tag.lcmv.pauken == c("SRR4235368", "SRR4235369", "SRR4235370",
                               "SRR4235371", "SRR4235372", "SRR4235373",
                               "SRR4235374", "SRR4235375", "SRR4235376",
                               "SRR4235377"))
bam.lcmv.pauken <- paste0(bam.lcmv.pauken.dir, "/", tag.lcmv.pauken, ".bam")
stopifnot(file.exists(bam.lcmv.pauken))

bam.lcmv.sen.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/chronic-ATAC-Sen2016/bam"
tag.lcmv.sen <- c("SRR4345662", "SRR4345663", "SRR4345664", "SRR4345665",
                  "SRR4345666", "SRR4345667", "SRR4345668", "SRR4345669",
                  "SRR4345670", "SRR4345671")
bam.lcmv.sen <- paste0(bam.lcmv.sen.dir, "/", tag.lcmv.sen, ".bam")
stopifnot(file.exists(bam.lcmv.sen))

bam.lcmv.scottbrowne.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/chronic-ATAC-Mognol2017/bam"
tag.lcmv.scottbrowne <- c("GSM2356795_GSM2356796", "GSM2356797",
                          "GSM2356798", "GSM2356799", "GSM2356780_GSM2356781",
                          "GSM2356782", "GSM2356784", "GSM2356785",
                          "GSM2356786", "GSM2356787", "GSM2356788",
                          "GSM2356810", "GSM2356811", "GSM2356812",
                          "GSM2356813", "GSM2356814", "GSM2356815")
bam.lcmv.scottbrowne <- paste0(bam.lcmv.scottbrowne.dir, "/",
                               tag.lcmv.scottbrowne, ".bam")
stopifnot(file.exists(bam.lcmv.scottbrowne))

bam.lcmv.scharer.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/chronic-ATAC-Schrarer2017/bam"
tag.lcmv.scharer <- c("GSM2192618", "GSM2192619", "GSM2192620", "GSM2192621",
                       "GSM2192622", "GSM2192623", "GSM2192624", "GSM2192625",
                       "GSM2192626", "GSM2192627", "GSM2192628", "GSM2192629",
                       "GSM2192630", "GSM2192631", "GSM2192632", "GSM2192633",
                       "GSM2192634")
bam.lcmv.scharer <- paste0(bam.lcmv.scharer.dir, "/",
                           tag.lcmv.scharer, ".bam")
stopifnot(file.exists(bam.lcmv.scharer))

bam.lcmv.miller.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/chronic-tumor-ATAC-Miller2019/bam"
tag.lcmv.miller <- c("SRR8269961", "SRR8269962", "SRR8269963", "SRR8269964",
                     "SRR8269965", "SRR8269966", "SRR8269967", "SRR8269968",
                     "SRR8269969", "SRR8269970", "SRR8269971", "SRR8269972")
bam.lcmv.miller <- paste0(bam.lcmv.miller.dir, "/",
                          tag.lcmv.miller, ".bam")
stopifnot(file.exists(bam.lcmv.miller))


bam.til.mognol.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/chronic-ATAC-Mognol2017/bam"
tag.til.mognol <- c("GSM2442543", "GSM2442544", "GSM2442541", "GSM2442542",
                    "GSM2442535", "GSM2442536", "GSM2442537", "GSM2442538",
                    "GSM2442539", "GSM2442540")
bam.til.mognol <- paste0(bam.til.mognol.dir, "/", tag.til.mognol, ".bam")
stopifnot(file.exists(bam.til.mognol))

bam.til.philip.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/tumordysfunc-Philip2017/bam"
tag.til.philip <- c("N_L1", "N_L2", "N_L3",
                    "Eff_D5_L1", "Eff_D5_L2", "Eff_D5_L3",
                    "E_L2", "E_L3",
                    "FM1", "M_L2", "M_L3",
                    "D5_L1", "D5_L2", "D5_L3", "D5_L4",
                    "D7_L1", "D7_L2", "D7_L3",
                    "D14_L1", "D14_L2", "D14_L3",
                    "D21_L2", "D21_L3", "D21_L4",
                    "D28_L1", "D28_L2", "D28_L4",
                    "D35_L1", "D35_L2", "D35_L4",
                    "D60_L1", "D60_L2", "D60_L3",
                    "ML_D7_L1", "ML_D7_L2", "ML_D7_L3", "ML_D7_L4",
                    "ML_D14_L1", "ML_D14_L2",
                    "ML_D35_L1", "ML_D35_L2", "ML_D35_L3",
                    "ML_noLM_D7_L1", "ML_noLM_D7_L2", "ML_noLM_D7_L3",
                    "ML_noLM_D14_L1", "ML_noLM_D14_L2", "ML_noLM_D14_L3")
bam.til.philip <- paste0(bam.til.philip.dir, "/", tag.til.philip, ".bam")
stopifnot(file.exists(bam.til.philip))

bam.til.chen.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/til-cart-ATAC-Chen2019/bam"
tag.til.chen <- c("SRR8311283_SRR8311284", "SRR8311285_SRR8311286",
                   "SRR8311287_SRR8311288", "SRR8311289_SRR8311290",
                   "SRR8311291", "SRR8311292_SRR8311293", "SRR8311294",
                   "SRR8311295", "SRR8311296", "SRR8311297", "SRR8311298",
                   "SRR8311299", "SRR8311300", "SRR8311301",
                   "SRR8311302_SRR8311303", "SRR8311304_SRR8311305",
                   "SRR8311306", "SRR8311307_SRR8311308", "SRR8311309",
                   "SRR8311310", "SRR8311311", "SRR8311312", "SRR8311313",
                   "SRR8311314", "SRR8311315", "SRR8311316_SRR8311317",
                   "SRR8311318", "SRR8311319")
bam.til.chen <- paste0(bam.til.chen.dir, "/", tag.til.chen, ".bam")
stopifnot(file.exists(bam.til.chen))


bam.hybrid.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/hybrid-mouse-JorisYi/bam-ATAC/"
tag.hybrid <- c("nav_r1", "nav_r2", "nav_r3", "d07_r1", "d07_r2", "d07_r3",
                "d60_r1", "d60_r2", "d60_r3")
bam.hybrid <- paste0(bam.hybrid.dir, "/b6_spret_lcmv_cd8_", tag.hybrid, "_ATACseq.combined.rmDup.Aligned.sortedByCoord.out.bam")
stopifnot(file.exists(bam.hybrid))


bam.tissueres.milner.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/tissueresident-ATAC-Milner2017/bam"
tag.tissueres.milner <- c("SRR6324437", "SRR6324438", "SRR6324439",
                          "SRR6324440", "SRR6324441", "SRR6324442")
bam.tissueres.milner <- paste0(bam.tissueres.milner.dir, "/",
                               tag.tissueres.milner, ".bam")
stopifnot(file.exists(bam.tissueres.milner))

bam.effmem.yu.dir <- "/Volumes/Drive20/Projects-data/dysfuncTcells/data/effmem-ATAC-Yu2017/bam"
tag.effmem.yu <- c("SRR5305074", "SRR5305075", "SRR5305076", "SRR5305077",
                   "SRR5305078", "SRR5305079", "SRR5305080", "SRR5305081")
bam.effmem.yu <- paste0(bam.effmem.yu.dir, "/", tag.effmem.yu, ".bam")
stopifnot(file.exists(bam.effmem.yu))


bam.dnase.dir <- "/Volumes/Drive20/Projects-data/from-e63data/DNaseTcell/alignment/bam-dnase"
tag.dnase <- c("DS23186", "DS35472", "DS38764", "DS38771", "DS38772",
               "DS35737", "DS35741", "DS38779", "DS24597", "DS35477",
               "DS38769", "DS38777", "DS23988", "DS27832", "DS35475",
               "DS38767", "DS28289", "DS28293", "DS39247", "DS39249",
               "DS39251", "DS39717", "DS27890", "DS35749", "DS38868",
               "DS38869", "DS38871", "DS38873", "DS27835", "DS27869",
               "DS27870")
bam.dnase <- paste0(bam.dnase.dir, "/", tag.dnase, ".bam")
stopifnot(file.exists(bam.dnase))



all.atac.peaks <- readRDS("datafiles/all-atac-peaks.rds")
all.atac.summits <- readRDS("datafiles/all-atac-summits.rds")


print("count reads in combined ATAC peaks")
print("use files:")
all.bamfiles <- c(bam.lcmv.pauken, bam.lcmv.sen,
                  bam.lcmv.scottbrowne, bam.lcmv.scharer,
                  bam.lcmv.miller,
                  bam.til.mognol, bam.til.philip, bam.til.chen,
                  bam.hybrid,
                  bam.effmem.yu, bam.tissueres.milner,
                  bam.dnase)
atac.peaks.table <- as.data.frame(all.atac.peaks) %>%
    dplyr::select(GeneID = name, Chr = seqnames,
                  Start = start, End = end, Strand = strand)
atac.peak.counts.raw <-
    featureCounts(all.bamfiles, annot.ext = atac.peaks.table,
                  isPairedEnd = FALSE, fracOverlap = 0.3,
                  countChimericFragments = FALSE, nthreads = 10)
print("count complete, save the result")
saveRDS(atac.peak.counts.raw, "datafiles/counts-all-atac-peaks-raw.rds")


print("count reads in summits of combined ATAC peaks")
print("use files:")
atac.summits.table <- as.data.frame(all.atac.summits) %>%
    dplyr::select(GeneID = name, Chr = seqnames,
                  Start = start, End = end, Strand = strand)
atac.summit.counts.raw <-
    featureCounts(all.bamfiles, annot.ext = atac.summits.table,
                  isPairedEnd = FALSE, fracOverlap = 0.3,
                  allowMultiOverlap = TRUE,
                  countChimericFragments = FALSE, nthreads = 10)
print("count complete, save the result")
saveRDS(atac.summit.counts.raw, "datafiles/counts-all-atac-summits-raw.rds")


sessionInfo()
