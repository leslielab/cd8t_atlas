library(rtracklayer)
library(GenomicRanges)
library(tidyverse)
library(data.table)
library(DESeq2)
library(BiocParallel)


print("load data")
all.peaks <- readRDS("datafiles/selected-peaks.rds")
dds.peaks <- readRDS("datafiles/counts-all-atac-peaks-dds-withhybrid.rds")
dds.peaks <- dds.peaks[names(all.peaks), ]
dds.peaks$datasource <- droplevels(dds.peaks$datasource)
dds.peaks <- estimateSizeFactors(dds.peaks)


print("run initial DESeq")
register(MulticoreParam(3))
dds.peaks <- DESeq(dds.peaks, parallel = TRUE)
saveRDS(dds.peaks, "datafiles/dds-peaks-withhybrid-uncorrected.rds")
vsd.peaks <- varianceStabilizingTransformation(dds.peaks, blind = FALSE)


print("define function to learn models for batch effect")
#' Output of the function is DESeqDataSet with a trained model
#' that includes "datasource" factor. log2FoldChange values are estimated
#' for this factor in each sample and can be used for batch effect correction.
trainBatchModel <- function(dds, includeCondition = FALSE) {
    dds.batch <- dds
    if (includeCondition) {
        design(dds.batch) <- ~condition + datasource
    } else {
        design(dds.batch) <- ~datasource
    }
    dds.batch <- estimateSizeFactors(dds.batch)
    dds.batch <- DESeq(dds.batch, parallel = TRUE)
    dds.batch
}


print("define function to do batch correction")
#' dds is a DESeqDataSet where batch effect correction should be done,
#' dds.batch is a DESeqDataSet with "datasource" factor estimates such as
#' output by trainBatchModel()
#' Output of this function is DESeqDataSet with counts corrected
#' for batch effect (associated with factor "datasource")
correctBatch <- function(dds, dds.batch) {
    dds.batch <- dds.batch[rownames(dds), ]
    batch.levels <- levels(dds.batch$datasource)
    coef.names <- paste0("datasource_", batch.levels[2:length(batch.levels)],
                         "_vs_", batch.levels[1])
    log2FC.batch <- mcols(dds.batch)[, coef.names]
    log2FC.batch <- apply(log2FC.batch, 2,
                          function(x) {
                              x[is.na(x)] <- 0
                              x
                          })
    batch <- dds$datasource
    batch <- model.matrix(~batch)[, -1, drop = FALSE]
    log2FC.batch <- log2FC.batch %*% t(batch)
    dds.batchcorr.mat <- counts(dds) # , normalized = TRUE)
    dds.batchcorr.mat <- dds.batchcorr.mat / (2 ** log2FC.batch)
    dds.batchcorr <- dds
    assays(dds.batchcorr)[[1]] <- apply(dds.batchcorr.mat, 2, as.integer)
    dds.batchcorr <- estimateSizeFactors(dds.batchcorr)
    dds.batchcorr <- estimateDispersions(dds.batchcorr)
    # vsd.batchcorr <- varianceStabilizingTransformation(dds.batchcorr,
    #                                                    blind = FALSE)
    # return(list(dds.batchcorr = dds.batchcorr,
    #             vsd.batchcorr = vsd.batchcorr))
    dds.batchcorr
}

print("learn models of batch effect for peaks")
dds.peaks.condition.batch <- dds.peaks
dds.peaks.condition.batch <- trainBatchModel(dds.peaks.condition.batch,
                                             includeCondition = TRUE)
saveRDS(dds.peaks.condition.batch,
        "datafiles/dds-peaks-withhybrid-condition-batch.rds")

print("do batch correction for peaks")
dds.peaks.condition.batchcorr <-
    correctBatch(dds.peaks, dds.peaks.condition.batch)
saveRDS(dds.peaks.condition.batchcorr,
        "datafiles/dds-peaks-withhybrid-condition-batchcorr.rds")

sessionInfo()
