#!/bin/bash


echo "run IDR analysis for peaks"
echo
idr --version
echo

peaksdir="peaks-macs2"
idrdir="idr-results"


mkdir -p $idrdir

sort -k1,3 -u $peaksdir/merged/merged_peaks.narrowPeak | bedtools sort -i - >$idrdir/merged.narrowPeak


condition="naive"
sample1=SRR4235368
sample2=SRR4235369
idr --verbose --samples $peaksdir/"$sample1"/"$sample1"_peaks.narrowPeak $peaksdir/"$sample2"/"$sample2"_peaks.narrowPeak --peak-list $idrdir/merged.narrowPeak -o $idrdir/$condition.narrowPeak --log-output-file $idrdir/$condition.log.txt --plot
awk '$5 > 540' $idrdir/$condition.narrowPeak >$idrdir/$condition.idr.narrowPeak

condition="effector"
sample1=SRR4235370
sample2=SRR4235371
idr --verbose --samples $peaksdir/"$sample1"/"$sample1"_peaks.narrowPeak $peaksdir/"$sample2"/"$sample2"_peaks.narrowPeak --peak-list $idrdir/merged.narrowPeak -o $idrdir/$condition.narrowPeak --log-output-file $idrdir/$condition.log.txt --plot
awk '$5 > 540' $idrdir/$condition.narrowPeak >$idrdir/$condition.idr.narrowPeak

condition="memory"
sample1=SRR4235372
sample2=SRR4235373
idr --verbose --samples $peaksdir/"$sample1"/"$sample1"_peaks.narrowPeak $peaksdir/"$sample2"/"$sample2"_peaks.narrowPeak --peak-list $idrdir/merged.narrowPeak -o $idrdir/$condition.narrowPeak --log-output-file $idrdir/$condition.log.txt --plot
awk '$5 > 540' $idrdir/$condition.narrowPeak >$idrdir/$condition.idr.narrowPeak

condition="exhausted"
sample1=SRR4235374
sample2=SRR4235375
idr --verbose --samples $peaksdir/"$sample1"/"$sample1"_peaks.narrowPeak $peaksdir/"$sample2"/"$sample2"_peaks.narrowPeak --peak-list $idrdir/merged.narrowPeak -o $idrdir/$condition.narrowPeak --log-output-file $idrdir/$condition.log.txt --plot
awk '$5 > 540' $idrdir/$condition.narrowPeak >$idrdir/$condition.idr.narrowPeak

condition="exhausted-aPDL1"
sample1=SRR4235376
sample2=SRR4235377
idr --verbose --samples $peaksdir/"$sample1"/"$sample1"_peaks.narrowPeak $peaksdir/"$sample2"/"$sample2"_peaks.narrowPeak --peak-list $idrdir/merged.narrowPeak -o $idrdir/$condition.narrowPeak --log-output-file $idrdir/$condition.log.txt --plot
awk '$5 > 540' $idrdir/$condition.narrowPeak >$idrdir/$condition.idr.narrowPeak



echo "cat all reproducible peaks into one file"
rm -rf tmp.narrowPeak
for condition in naive effector memory exhausted exhausted-aPDL1
do
    cut -f1-3 $idrdir/$condition.idr.narrowPeak >>tmp.narrowPeak
done
echo "merge into one file $idrdir/merged.idr.narrowPeak"
bedtools sort -i tmp.narrowPeak >tmp2.narrowPeak
bedtools merge -i tmp2.narrowPeak >$idrdir/merged.idr.narrowPeak
rm -rf tmp.narrowPeak
rm -rf tmp2.narrowPeak

echo "find summits of reproducible peaks"
bedtools intersect -a $peaksdir/merged/merged_summits.bed -b $idrdir/merged.idr.narrowPeak >$idrdir/merged.idr.summit.bed


echo "IDR done"
