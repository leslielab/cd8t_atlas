#!/bin/bash

#PBS -l walltime=24:00:00
#PBS -l nodes=1:ppn=1
#PBS -l mem=50gb
#PBS -j oe
#PBS -k oe
#PBS -m n
##PBS -m abe
#PBS -V
#PBS -N macs2-atac-pauken2016
#PBS -o macs2-atac-pauken2016

cd $PBS_O_WORKDIR


echo "produce peaks using MACS2"
echo

echo "macs2 version:"
macs2 --version
echo

bamdir="/cbio/cllab/projects/pritykin/dysfuncTcells/chronic-ATAC-Pauken2016/bam"
beddir="/cbio/cllab/projects/pritykin/dysfuncTcells/chronic-ATAC-Pauken2016/peakcalling/tmpbed"
peaksdir="/cbio/cllab/projects/pritykin/dysfuncTcells/chronic-ATAC-Pauken2016/peakcalling/peaks-macs2"
bindir="/cbio/cllab/home/pritykin/bin"

mkdir -p $beddir
mkdir -p $peaksdir


awk_filt_prog='
BEGIN {OFS = FS}
{
 if ($1 != "chrM") {
  if ($6 == "+") {
      $2 = $2 + 4
  } else if ($6 == "-") {
      $3 = $3 - 5
  }
  print $0
 }
}
'

for prefix in SRR4235368 SRR4235369 SRR4235370 SRR4235371 SRR4235372 SRR4235373 SRR4235374 SRR4235375 SRR4235376 SRR4235377
do
    bamfile=$bamdir/$prefix.bam
    bedfile=$beddir/$prefix.bed
    bedfileshifted=$beddir/$prefix.shifted.bed
    macsdir="$peaksdir/$prefix/"
    mkdir -p $macsdir
    
    echo "map BAM to BED for $prefix"
    $bindir/bedtools bamtobed -i $bamfile >$bedfile

    echo "shift BED for $prefix"
    awk -F $'\t' "$awk_filt_prog" $bedfile >$bedfileshifted

    echo "run macs2 callpeak for $prefix"
    macs2 callpeak -t $bedfileshifted -f BED -g hs --outdir $macsdir --nomodel --shift 0 --extsize 76 --name $prefix -p 0.1 -B --SPMR --keep-dup 'auto' --call-summits 2>&1 > $macsdir/.macs
    echo "done"
    echo
done



macsdir="$peaksdir/merged/"
mkdir -p $macsdir

macs2 callpeak -t $beddir/SRR4235368.shifted.bed $beddir/SRR4235369.shifted.bed $beddir/SRR4235370.shifted.bed $beddir/SRR4235371.shifted.bed $beddir/SRR4235372.shifted.bed $beddir/SRR4235373.shifted.bed $beddir/SRR4235374.shifted.bed $beddir/SRR4235375.shifted.bed $beddir/SRR4235376.shifted.bed $beddir/SRR4235377.shifted.bed -f BED -g hs --outdir $macsdir --nomodel --shift 0 --extsize 76 --name merged -p 0.1 -B --SPMR --keep-dup 'auto' --call-summits 2>&1 > $macsdir/.macs
