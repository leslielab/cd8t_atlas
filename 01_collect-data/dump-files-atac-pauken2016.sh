#!/bin/bash

files=`cat SRR_Acc_List.txt`
for file in $files
do
    fastq-dump -I --split-files --gzip -A $file
done
