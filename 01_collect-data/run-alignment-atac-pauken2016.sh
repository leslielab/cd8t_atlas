#!/bin/bash

nthreads=12

mkdir -p bowtie2-output
mkdir -p bam
mkdir -p bam/flagstat
mkdir -p bam/fastqc-output

for prefix in `cat SRR_Acc_List.txt`
do
    nice -n 10 bowtie2 -p $nthreads -t --no-unal -X 1000 --no-mixed --no-discordant -x mm10-bowtie2index/mm10 -1 "$prefix"_1.fastq.gz -2 "$prefix"_2.fastq.gz >bowtie2-output/$prefix.sam 2>bowtie2-output/.bowtie2.$prefix
    samtools view -h -bS -F 4 -q 20 bowtie2-output/$prefix.sam >bowtie2-output/$prefix.unsorted.bam
    rm -rf bowtie2-output/$prefix.sam

    samtools sort bowtie2-output/$prefix.unsorted.bam >bam/$prefix.bam
    rm -rf bowtie2-output/$prefix.unsorted.bam

    samtools index bam/$prefix.bam
    samtools flagstat bam/$prefix.bam >bam/flagstat/$prefix.flagstat
    nice -n 10 fastqc -q -t $nthreads -o bam/fastqc-output bam/$prefix.bam
done
