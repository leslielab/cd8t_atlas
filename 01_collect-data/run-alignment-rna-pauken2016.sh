#!/bin/bash

#BSUB -J hisat2-rna-pauken2016
#BSUB -n 30
#BSUB -R span[ptile=30]
#BSUB -R rusage[mem=0.3]
#BSUB -W 06:00
#BSUB -o loglilac.%J.%I.stdout
#BSUB -eo loglilac.%J.%I.stderr


## run this script as follow:
## bsub -J "run-alignment[1-13]%13" <run-alignment.sh
## then use variable $LSB_JOBINDEX within the script

nthreads=30
mybin="/home/pritykin/bin"
maindir="/data/leslie/pritykin/projects/dysfuncTcells/data"
raw_data_dir="$maindir/chronic-RNA-Pauken2016"
ht2index="$maindir/genome/mm10-hisat2-index/mm10"
splicesites="$maindir/genome/mm10-hisat2-index/gencode.vM14.annotation_hisat2_splicesites.txt"
align_dir="$raw_data_dir/bam"
hisat2_output_dir="$raw_data_dir/hisat2-output"
fastqc_bam_dir="$raw_data_dir/bam/fastqc-output"


mkdir -p $align_dir
mkdir -p $hisat2_output_dir
mkdir -p $fastqc_bam_dir


sratags=(`tail -n+2 $raw_data_dir/SraRunTable.txt | cut -f7 | sort | uniq`)
sratag=${sratags[$LSB_JOBINDEX-1]}


samfile=${hisat2_output_dir}/$sratag.sam
bamfileprefix=${align_dir}/$sratag

echo "align $sratag"
fastq="$raw_data_dir"/"$sratag".fastq.gz
echo "use files $fastq"

$mybin/hisat2 -p $nthreads -t --no-unal --known-splicesite-infile $splicesites -x $ht2index -U "$fastq" >$samfile 2>${hisat2_output_dir}/.hisat2.$sratag
echo "alignment of $sratag done"

echo "filtering for uniquely aligned reads..."
grep -v 'NH:i:[2-9]' $samfile | $mybin/samtools view -h -b -F 4 -q 20 >$bamfileprefix.unsorted.bam
rm -rf $samfile
echo "done"

echo "samtools sort..."
$mybin/samtools sort -@ 10 $bamfileprefix.unsorted.bam >$bamfileprefix.bam
rm -rf $bamfileprefix.unsorted.bam
echo "done"

echo "samtools index..."
$mybin/samtools index $bamfileprefix.bam
echo "done"

echo "fastqc..."
$mybin/fastqc -q -t $nthreads -o ${fastqc_bam_dir} $bamfileprefix.bam
echo "done"
