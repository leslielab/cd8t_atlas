library(rtracklayer)
library(GenomicRanges)
library(tidyverse)
library(data.table)
library(DESeq2)
library(pheatmap)
library(BiocParallel)


dir.create("plots", showWarnings = FALSE)
dir.create("plots/pca", showWarnings = FALSE)
dir.create("plots/maplot", showWarnings = FALSE)
dir.create("datafiles", showWarnings = FALSE)


print("load data")
dds.pauken <- readRDS("datafiles/dds-pauken2016.rds")
dds.scottbrowne <- readRDS("datafiles/dds-scottbrowne2016.rds")
dds.mognol <- readRDS("datafiles/dds-mognol2017.rds")
dds.man <- readRDS("datafiles/dds-man2017.rds")
dds.utzschneider <- readRDS("datafiles/dds-utzschneider2016.rds")
dds.philip <- readRDS("datafiles/dds-philip2017.rds")
dds.miller <- readRDS("datafiles/dds-miller2019.rds")
dds.chen <- readRDS("datafiles/dds-chen2019.rds")
dds.acute.chen <- readRDS("datafiles/dds-acute-chen2019.rds")

dds.rna.corrected <-
    readRDS("datafiles/dds-rna-selected-condition-batchcorr.rds")
dds.rna.corrected <- estimateSizeFactors(dds.rna.corrected)
min.count <- 10
dds.rna.corrected <-
    dds.rna.corrected[rowMeans(counts(dds.rna.corrected,
                                      normalized = TRUE)) > min.count, ]
expressed.genes <- rownames(dds.rna.corrected)
# vsd.rna <- varianceStabilizingTransformation(dds.rna)
dds.rna <- readRDS("datafiles/dds-rna-selected-uncorrected.rds")
dds.rna <- dds.rna[expressed.genes, ]


register(MulticoreParam(6))
runDESeq <- function(dds, tag = "", mode = 1) {
    print(sprintf("Run DESeq for %s", tag))
    dds$cellstate <- droplevels(dds$cellstate)
    dds$condition <- droplevels(dds$condition)
    dds$dysfunctime <- droplevels(dds$dysfunctime)
    dds$datasource <- droplevels(dds$datasource)
    if (mode == 1) {
        design(dds) <- ~datasource + cellstate
    } else if (mode == 2) {
        design(dds) <- ~datasource + condition
    } else if (mode == 3) {
        design(dds) <- ~condition
    } else if (mode == 4) {
        design(dds) <- ~datasource + dysfunctime
    } else if (mode == 5) {
        design(dds) <- ~dysfunctime
    } else if (mode == 6) {
        design(dds) <- ~population
    } else if (mode == 7) {
        design(dds) <- ~genotype
    } else if (mode == 8) {
        design(dds) <- ~tfhlike
    } else if (mode == 9) {
        design(dds) <- ~cxcr5
    } else if (mode == 10) {
        dds$challenge <- factor(dds$cellstate == "dysfunc_lcmv")
        design(dds) <- ~challenge
    } else if (mode == 11) {
        design(dds) <- ~cellstate
    }
    dds <- estimateSizeFactors(dds)
    normcounts <- counts(dds, normalized = TRUE)
    min.count <- 10
    dds <- dds[rowMax(normcounts) > min.count, ]
    print("use this filtered data set:")
    print(dds)
    dds <- estimateSizeFactors(dds)
    dds <- DESeq(dds, parallel = TRUE)
    res <- results(dds, parallel = TRUE)
    filename <- sprintf("plots/maplot/maplot-%s.png", tag)
    png(filename)
    plotMA(res)
    dev.off()
    print("summary of results:")
    print(summary(res))
    res.dt <- as.data.table(res)
    res.dt$gene <- rownames(res)
    print("top results:")
    print(res.dt[order(padj)][1:50, ])
    print(sprintf("analysis of %s done", tag))
    print("")
    print("")
    list(dds = dds, res = res.dt)
}


print("find differentially expressed genes")
dds.cellstate.list <-
    list("naive-effector" =
             dds.rna[, dds.rna$cellstate %in% c("naive", "effector")],
         "effector-memory" =
             dds.rna[, dds.rna$cellstate %in% c("effector", "memory")],
         "naive-memory" =
             dds.rna[, dds.rna$cellstate %in% c("naive", "memory")])

for (tag in names(dds.cellstate.list)) {
    deseq.results <- runDESeq(dds.cellstate.list[[tag]], tag, mode = 1)
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}


dds.condition.list <-
    list("naive-dysfunc" =
             dds.rna[, dds.rna$condition %in% c("naive", "dysfunc")],
         "naive-func" =
             dds.rna[, dds.rna$condition %in% c("naive", "func")],
         "func-dysfunc" =
             dds.rna[, dds.rna$condition %in% c("func", "dysfunc")],
         "naive-dysfuncearly" =
             dds.rna[, (dds.rna$condition == "naive" |
                     (dds.rna$cellstate %in% c("dysfunc_lcmv_early",
                                            "dysfunc_hepatocarc_progr_early") &
                          dds.rna$treatment %in% c("no", "control") &
                          dds.rna$dysfunctime == "early"))  &
                         dds.rna$datasource %in%
                         c("Pauken2016", "ScottBrowne2016", "Man2017",
                           "Utzschneider2016", "Philip2017")],
         "naive-dysfuncearly-lcmv" =
             dds.rna[, (dds.rna$condition == "naive" |
                        (dds.rna$cellstate == "dysfunc_lcmv_early" &
                             dds.rna$treatment %in% c("no", "control") &
                             dds.rna$dysfunctime == "early")) &
                     dds.rna$datasource %in% c("Pauken2016", "ScottBrowne2016",
                                               "Man2017", "Utzschneider2016")],
         "effector-dysfuncearly" =
             dds.rna[, (dds.rna$cellstate == "effector" |
                     (dds.rna$cellstate %in% c("dysfunc_lcmv_early",
                                            "dysfunc_hepatocarc_progr_early") &
                          dds.rna$treatment %in% c("no", "control") &
                          dds.rna$dysfunctime == "early")) &
                         dds.rna$datasource %in%
                         c("ScottBrowne2016", "Man2017", "Philip2017")],
         "memory-dysfunclate" =
             dds.rna[, dds.rna$cellstate == "memory" |
                     (dds.rna$cellstate %in% c("dysfunc_lcmv",
                                               "dysfunc_hepatocarc_progr") &
                          dds.rna$treatment %in% c("no", "control") &
                          dds.rna$dysfunctime == "late")])

for (tag in names(dds.condition.list)) {
    deseq.results <- runDESeq(dds.condition.list[[tag]], tag, mode = 2)
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}


print("find differentially expressed genes between dysfunctional cells in tumors and chronic infection")
dds.list <-
    list("dysfunc-tumor-chronic-late" =
             dds.rna.corrected[, (dds.rna.corrected$condition == "dysfunc") &
                     !is.na(dds.rna.corrected$dysfunctime == "late") &
                     (dds.rna.corrected$dysfunctime == "late") &
                     (dds.rna.corrected$treatment == "no")],
         "dysfunc-tumor-chronic" =
             dds.rna.corrected[, (dds.rna.corrected$condition == "dysfunc") &
                     (is.na(dds.rna.corrected$dysfunctime == "late") |
                          (dds.rna.corrected$dysfunctime == "late")) &
                     (dds.rna.corrected$treatment == "no")],
         "dysfunc-hepatocarc-chronic" =
             dds.rna.corrected[,
                        (dds.rna.corrected$cellstate %in% c("dysfunc_lcmv",
                                         "dysfunc_hepatocarc_progr")) &
                     !is.na(dds.rna.corrected$dysfunctime == "late") &
                     (dds.rna.corrected$dysfunctime == "late") &
                     (dds.rna.corrected$treatment == "no")],
         "dysfunc-melanoma-chronic" =
             dds.rna.corrected[,
                        (dds.rna.corrected$cellstate %in% c("dysfunc_lcmv",
                                         "dysfunc_melanoma")) &
                     (is.na(dds.rna.corrected$dysfunctime == "late") |
                          (dds.rna.corrected$dysfunctime == "late")) &
                     (dds.rna.corrected$treatment == "no")])

for (tag in names(dds.list)) {
    deseq.results <- runDESeq(dds.list[[tag]], tag, mode = 10)
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}



dds.condition.onestudy.list <-
    list("naive-dysfuncearly-hepatocarc" =
             dds.rna[, (dds.rna$condition == "naive" |
                    dds.rna$cellstate == "dysfunc_hepatocarc_progr_early") &
                     dds.rna$datasource == "Philip2017"])

for (tag in names(dds.condition.onestudy.list)) {
    deseq.results <- runDESeq(dds.condition.onestudy.list[[tag]], tag, mode = 3)
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}
dds.condition.onestudy.list <-
    list("dysfunc-cart-endogenous-progenitor" =
             dds.rna[, dds.rna$cellstate %in% c("dysfunc_melanoma_cart_progenitor",
                                        "dysfunc_melanoma_progenitor") &
                     dds.rna$datasource == "Chen2019"],
         "dysfunc-cart-endogenous-terminal" =
             dds.rna[, dds.rna$cellstate %in% c("dysfunc_melanoma_cart",
                                        "dysfunc_melanoma") &
                     dds.rna$datasource == "Chen2019"])
for (tag in names(dds.condition.onestudy.list)) {
    deseq.results <- runDESeq(dds.condition.onestudy.list[[tag]], tag,
                              mode = 11)
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}


dds.dysfunctime.list <-
    list("dysfunc-earlylate" =
             dds.rna[, dds.rna$cellstate %in% c("dysfunc_lcmv_early",
                                                "dysfunc_lcmv",
                                            "dysfunc_hepatocarc_progr_early",
                                            "dysfunc_hepatocarc_progr")  &
                     dds.rna$treatment %in% c("no", "control")],
         "dysfunc-earlylate-lcmv" =
             dds.rna[, dds.rna$cellstate %in% c("dysfunc_lcmv_early",
                                                "dysfunc_lcmv") &
                     dds.rna$treatment %in% c("no", "control")],
         "dysfunc-progenitor-terminal" =
             dds.rna[, dds.rna$cellstate %in%
                         c("dysfunc_lcmv_progenitor",
                           "dysfunc_lcmv",
                           "dysfunc_melanoma_progenitor",
                           "dysfunc_melanoma",
                           "dysfunc_melanoma_cart_progenitor",
                           "dysfunc_melanoma_cart")  &
                     dds.rna$datasource %in% c("Chen2019", "Miller2019",
                                               "Utzschneider2016") &
                     dds.rna$treatment %in% c("no", "control")],
         "dysfunc-progenitor-terminal-melanoma" =
             dds.rna[, dds.rna$cellstate %in% c("dysfunc_melanoma_progenitor",
                                                "dysfunc_melanoma")  &
                     dds.rna$datasource %in% c("Chen2019", "Miller2019") &
                     dds.rna$treatment %in% c("no", "control")],
         "dysfunc-progenitor-terminal-lcmv" =
             dds.rna[, dds.rna$cellstate %in% c("dysfunc_lcmv_progenitor",
                                                "dysfunc_lcmv") &
                         dds.rna$datasource %in% c("Utzschneider2016",
                                                   "Miller2019")])
for (tag in c("dysfunc-progenitor-terminal",
              "dysfunc-progenitor-terminal-melanoma")) {
    prog.term <- dds.dysfunctime.list[[tag]]$dysfunctime
    prog.term <- factor(ifelse(prog.term %in% "progenitor",
                               "progenitor", "terminal"),
                        levels = c("progenitor", "terminal"))
    dds.dysfunctime.list[[tag]]$dysfunctime <- prog.term
}

for (tag in names(dds.dysfunctime.list)) {
    deseq.results <- runDESeq(dds.dysfunctime.list[[tag]], tag, mode = 4)
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}


dds.dysfunctime.onestudy.list <-
    list("dysfunc-earlylate-hepatocarc" =
             dds.rna[, dds.rna$cellstate %in%
                         c("dysfunc_hepatocarc_progr_early",
                           "dysfunc_hepatocarc_progr")],
         "dysfunc-progenitor-terminal-lcmv-miller2019" =
             dds.rna[, dds.rna$cellstate %in% c("dysfunc_lcmv_progenitor",
                                                "dysfunc_lcmv")  &
                       dds.rna$datasource %in% "Miller2019"],
         "dysfunc-progenitor-terminal-lcmv-utzschneider2016" =
             dds.rna[, dds.rna$cellstate %in% c("dysfunc_lcmv_progenitor",
                                                "dysfunc_lcmv")  &
                         dds.rna$datasource %in% "Utzschneider2016"],
         "dysfunc-progenitor-terminal-melanoma-cart" =
             dds.rna[, dds.rna$cellstate %in%
                         c("dysfunc_melanoma_cart_progenitor",
                           "dysfunc_melanoma_cart")],
         "dysfunc-progenitor-terminal-melanoma-chen2019" =
             dds.rna[, dds.rna$cellstate %in%
                         c("dysfunc_melanoma_progenitor",
                           "dysfunc_melanoma")  &
                     dds.rna$datasource %in% "Chen2019"],
         "dysfunc-progenitor-terminal-melanoma-miller2019" =
             dds.rna[, dds.rna$cellstate %in%
                         c("dysfunc_melanoma_progenitor",
                           "dysfunc_melanoma")  &
                     dds.rna$datasource %in% "Miller2019" &
                     dds.rna$treatment %in% c("no", "control")])
for (tag in c("dysfunc-progenitor-terminal-melanoma-cart",
              "dysfunc-progenitor-terminal-melanoma-chen2019")) {
    prog.term <- dds.dysfunctime.onestudy.list[[tag]]$dysfunctime
    prog.term <- factor(ifelse(prog.term %in% "progenitor",
                               "progenitor", "terminal"))
    dds.dysfunctime.onestudy.list[[tag]]$dysfunctime <- prog.term
}

for (tag in names(dds.dysfunctime.onestudy.list)) {
    deseq.results <- runDESeq(dds.dysfunctime.onestudy.list[[tag]],
                              tag, mode = 5)
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}


print("compare expression in Tcf1+ and Tcf1- populations in Utzschneider2016")
tag <- "utzschneider2016-pairwise-tcf1-memory"
dds.tcf1.memory <- dds.utzschneider[expressed.genes,
                                    dds.utzschneider$cellstate == "memory"]
cell.populations <- c("Tcf1minus", "Tcf1plus")
names(cell.populations) <- c("Tcf1-", "Tcf1+")
dds.tcf1.memory$population <-
    factor(cell.populations[dds.tcf1.memory$sorting],
           levels = c("Tcf1plus", "Tcf1minus"))
deseq.results <- runDESeq(dds.tcf1.memory, tag, mode = 6)
saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))


print("compare expression in Irf4Het, BatfHet and WT populations in Man2017")
dds.man.list <-
    list("man2017-pairwise-irf4-effector" =
             dds.man[, dds.man$treatment %in% c("no", "control") &
                       dds.man$cellstate == "effector" &
                       dds.man$genotype %in% c("WT", "Irf4Het")],
         "man2017-pairwise-batf-effector" =
             dds.man[, dds.man$treatment %in% c("no", "control") &
                       dds.man$cellstate == "effector" &
                       dds.man$genotype %in% c("WT", "BatfHet")],
         "man2017-pairwise-irf4-memory" =
             dds.man[, dds.man$treatment %in% c("no", "control") &
                       dds.man$cellstate == "memory"],
         "man2017-pairwise-irf4-chronic-early" =
             dds.man[, dds.man$treatment %in% c("no", "control") &
                       dds.man$cellstate == "dysfunc_lcmv_early" &
                       dds.man$dysfunctime == "early" &
                       dds.man$genotype %in% c("WT", "Irf4Het")],
         "man2017-pairwise-batf-chronic-early" =
             dds.man[, dds.man$treatment %in% c("no", "control") &
                         dds.man$cellstate == "dysfunc_lcmv_early" &
                         dds.man$dysfunctime == "early" &
                         dds.man$genotype %in% c("WT", "BatfHet")],
         "man2017-pairwise-irf4-chronic-late" =
             dds.man[, dds.man$treatment %in% c("no", "control") &
                         dds.man$cellstate == "dysfunc_lcmv" &
                         dds.man$dysfunctime == "late" &
                         dds.man$genotype %in% c("WT", "Irf4Het")])

for (tag in names(dds.man.list)) {
    dds <- dds.man.list[[tag]]
    dds$genotype <- factor(dds$genotype, levels = c("WT", "Irf4Het", "BatfHet"))
    dds$genotype <- droplevels(dds$genotype)
    deseq.results <- runDESeq(dds, tag, mode = 7)
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}



print("compare expression in Cxcr5+Pd1+, Cxcr5+Pd1- and Cxcr5-Pd1- WT populations in Chen et al. Nat Commun 2019")
dds.acute.chen.list <-
    list("acute-chen2019-Cxcr5+Pd1-_vs_Cxcr5-Pd1-" =
             dds.acute.chen[, dds.acute.chen$genotype %in%
                                c("Cxcr5negPd1neg", "Cxcr5posPd1neg")],
         "acute-chen2019-Cxcr5+Pd1+_vs_Cxcr5-Pd1-" =
             dds.acute.chen[, dds.acute.chen$genotype %in%
                                c("Cxcr5negPd1neg", "Cxcr5posPd1pos")],
         "acute-chen2019-Cxcr5+Pd1+_vs_Cxcr5+Pd1-" =
             dds.acute.chen[, dds.acute.chen$genotype %in%
                                c("Cxcr5posPd1neg", "Cxcr5posPd1pos")],
         "acute-chen2019-Cxcr5+Pd1+_vs_other" = dds.acute.chen,
         "acute-chen2019-Cxcr5+_vs_other" = dds.acute.chen)

for (tag in names(dds.acute.chen.list)) {
    dds <- dds.acute.chen.list[[tag]]
    dds$dysfunctime <- factor(dds$dysfunctime)
    dds$genotype <- droplevels(dds$genotype)
    if (tag == "acute-chen2019-Cxcr5+Pd1+_vs_other") {
        deseq.results <- runDESeq(dds, tag, mode = 8)
    } else if (tag == "acute-chen2019-Cxcr5+_vs_other") {
        deseq.results <- runDESeq(dds, tag, mode = 9)
    } else {
        deseq.results <- runDESeq(dds, tag, mode = 7)
    }
    saveRDS(deseq.results$dds, sprintf("datafiles/dds-%s.rds", tag))
    saveRDS(deseq.results$res, sprintf("datafiles/res-%s.rds", tag))
}



sessionInfo()
