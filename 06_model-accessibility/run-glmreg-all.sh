#!/bin/bash

#BSUB -J run-glmreg
#BSUB -n 5
#BSUB -R span[ptile=5]
#BSUB -R rusage[mem=3]
#BSUB -W 150:00
#BSUB -o loglilac.%J.%I.stdout
#BSUB -eo loglilac.%J.%I.stderr


## run this script as follow:
## bsub -J "run-glmreg-all[1-146]%146" <run-glmreg-all.sh
## for extended version:
## bsub -J "run-glmreg-all[1-166]%166" <run-glmreg-all.sh
## then use variable $LSB_JOBINDEX within the script



fitdir="glm-fits-top152012"
fitoutputdir="$fitdir/output"

mkdir -p "$fitdir"
mkdir -p "$fitoutputdir"

i=$LSB_JOBINDEX

Rscript --vanilla glmreg-one-sample.R $i >"$fitoutputdir/.glmreg-$i" 2>"$fitoutputdir/.err.glmreg-$i"
