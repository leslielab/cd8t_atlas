library(data.table)
library(tidyverse)
library(mpath)
library(glmnet)
library(DESeq2)


min.count <- 1

n.targets <- 100

summits <- readRDS("datafiles/selected-summits.rds")

tf.hits.all <- readRDS("datafiles/fimo-motif-hits-merged.rds")
tf.hits <-
    tf.hits.all[, .(score = sum(score)), by = c("Motif_Name", "summit")]


dds.atac <- readRDS("datafiles/dds-summits-condition-batchcorr.rds")
dispersions.all <- mcols(dds.atac)[, "dispFit"]
names(dispersions.all) <- rownames(dds.atac)


normcounts.atac <- counts(dds.atac, normalized = TRUE)
normcounts.atac.dt <- as.data.table(normcounts.atac)
normcounts.atac.dt$name <- rownames(dds.atac)
normcounts.atac.dt <- gather(normcounts.atac.dt, sample, count, -name)
normcounts.atac.dt <- as.data.table(normcounts.atac.dt)
normcounts.atac.dt <-
    merge(normcounts.atac.dt,
          as.data.table(colData(dds.atac))[, .(sample, label)],
          by = "sample")
normcounts.atac.dt$count <- as.numeric(normcounts.atac.dt$count)
normcounts.atac.dt[, count := mean(count), by = c("name", "label")]
normcounts.atac.dt[, count := max(count), by = "name"]
normcounts.atac.dt <- unique(normcounts.atac.dt[, .(name, count)])

summits.top <- normcounts.atac.dt[count > min.count, name]
tf.hits.selected <- tf.hits[summit %in% summits.top]
tf.hits.selected[, N := .N, by = Motif_Name]
tf.hits.selected <- tf.hits.selected[N > n.targets]
tf.hits.selected[, N := NULL]
tf.score <- as.data.frame(spread(tf.hits.selected, "Motif_Name", "score",
                                 fill = 0))
summits.selected <- intersect(summits.top, tf.score$summit)

counts.atac <- counts(dds.atac)
counts.selected <- counts.atac[summits.selected, ]
rownames(tf.score) <- tf.score$summit
tf.score <- tf.score[summits.selected, ]
tf.score <- as.matrix(tf.score[-1])
dispersions.selected <- dispersions.all[summits.selected]


n.summits <- length(summits.selected)

dir.name <- sprintf("glm-fits-top%s", n.summits)

dir.create(dir.name, showWarnings = FALSE)
dir.create(sprintf("%s/data", dir.name), showWarnings = FALSE)


print(sprintf("write all data to files for top %s summits", n.summits))
tf.score.file <- sprintf("%s/data/tf-score.rds", dir.name)
saveRDS(tf.score, tf.score.file)
y.all.file <- sprintf("%s/data/y-all.rds", dir.name)
saveRDS(counts.selected, y.all.file)
disp.file <- sprintf("%s/data/dispersions.rds", dir.name)
saveRDS(dispersions.selected, disp.file)
for (i in 1:ncol(counts.selected)) {
    y.i.file <- sprintf("%s/data/y-%s.rds", dir.name, i)
    y.i <- counts.selected[, i]
    saveRDS(y.i, y.i.file)
}

sessionInfo()
